<?php

declare(strict_types = 1);

namespace CustomIS\ControllerUtilsBundle\ActionParam;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FormRequest
 *
 * @package CustomIS\ControllerUtilsBundle\ActionParam
 */
interface FormRequestInterface
{
    /**
     * @param string            $formType #Class
     * @param array|object|null $bindData
     * @param array             $options
     * @param Request|null      $request
     *
     * @return bool
     */
    public function handle(string $formType, $bindData = null, array $options = [], ?Request $request = null): bool;

    /**
     * @return mixed
     */
    public function getValidData();

    /**
     * @return FormView
     */
    public function createFormView(): FormView;

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface;

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return bool
     */
    public function isBound(): bool;
}
