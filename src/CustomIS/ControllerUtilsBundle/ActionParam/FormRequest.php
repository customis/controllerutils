<?php

declare(strict_types = 1);

namespace CustomIS\ControllerUtilsBundle\ActionParam;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FormRequest
 *
 * @package CustomIS\ControllerUtilsBundle\ActionParam
 */
class FormRequest implements FormRequestInterface
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var FormInterface|null
     */
    private $form;

    /**
     * FormRequest constructor.
     *
     * @param FormFactoryInterface $formFactory
     * @param RequestStack         $requestStack
     */
    public function __construct(FormFactoryInterface $formFactory, RequestStack $requestStack)
    {
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
    }

    /**
     * @param string            $formType #Class
     * @param array|object|null $bindData
     * @param array             $options
     * @param Request|null      $request
     *
     * @return bool
     */
    public function handle(string $formType, $bindData = null, array $options = [], ?Request $request = null): bool
    {
        $this->form = $this->formFactory->create($formType, $bindData, $options);
        $this->getForm()->handleRequest($request ?? $this->requestStack->getCurrentRequest());

        return $this->getForm()->isSubmitted() && $this->form->isValid();
    }

    /**
     * @return mixed
     */
    public function getValidData()
    {

        return $this->getForm()->getData();
    }

    /**
     * @return FormView
     */
    public function createFormView(): FormView
    {
        return $this->getForm()->createView();
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        if ($this->form === null) {
            throw new \RuntimeException('Symfony form is not initialized');
        }

        return $this->form;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->getForm()->isValid();
    }

    /**
     * @return bool
     */
    public function isBound(): bool
    {
        return $this->getForm()->isSubmitted();
    }
}
