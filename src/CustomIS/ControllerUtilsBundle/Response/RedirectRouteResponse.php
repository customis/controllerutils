<?php

declare(strict_types = 1);

namespace CustomIS\ControllerUtilsBundle\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class RedirectRouteResponse
 *
 * @package CustomIS\ControllerUtilsBundle\Response
 */
class RedirectRouteResponse
{
    /**
     * @var string
     */
    private $routeName;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var array
     */
    private $headers;

    /**
     * RedirectRouteResponse constructor.
     *
     * @param string $routeName  #Route
     * @param array  $parameters
     * @param int    $statusCode
     * @param array  $headers
     */
    public function __construct(string $routeName, array $parameters = [], int $statusCode = Response::HTTP_FOUND, array $headers = [])
    {
        $this->routeName = $routeName;
        $this->parameters = $parameters;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
