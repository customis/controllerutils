<?php

declare(strict_types = 1);

namespace CustomIS\ControllerUtilsBundle\EventListener;

use CustomIS\ControllerUtilsBundle\MVC\TemplateView;
use Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Templating\TemplateReferenceInterface;

/**
 * Class ViewListener
 *
 * @package CustomIS\ControllerUtilsBundle\EventListener
 */
class ViewListener implements EventSubscriberInterface
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var TemplateGuesser
     */
    private $guesser;

    /**
     * ViewListener constructor.
     *
     * @param EngineInterface $templating
     * @param TemplateGuesser $guesser
     */
    public function __construct(EngineInterface $templating, TemplateGuesser $guesser)
    {
        $this->templating = $templating;
        $this->guesser = $guesser;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => ['onKernelController', -128],
            KernelEvents::VIEW       => 'onKernelView',
        ];
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        $request = $event->getRequest();
        $template = $request->attributes->get('_template');
        if (null === $template) {
            try {
                $request->attributes->set('_template', $this->guesser->guessTemplateName(
                    $event->getController(),
                    $request
                ));
            } catch (\InvalidArgumentException $e) {
            }
        }
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event): void
    {
        $params = $event->getControllerResult();
        $request = $event->getRequest();

        /** @var TemplateReferenceInterface $template */
        $template = $request->attributes->get('_template');

        if (null === $template) {
            return;
        }

        $status = 200;
        $headers = [];
        if ($params instanceof TemplateView) {
            $template->set('name', $params->getActionTemplateName());
            $status = $params->getStatusCode();
            $headers = $params->getHeaders();
            $params = $params->getViewParams();
        }

        $event->setResponse(new Response($this->templating->render($template, $params), $status, $headers));
    }
}
