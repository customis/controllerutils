<?php

declare(strict_types = 1);

namespace CustomIS\ControllerUtilsBundle\EventListener;

use CustomIS\ControllerUtilsBundle\Response\RedirectRouteResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class RedirectRouteListner
 *
 * @package CustomIS\ControllerUtilsBundle\EventListener
 */
class RedirectRouteListner
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * RedirectRouteListner constructor.
     *
     * @param UrlGeneratorInterface $router
     */
    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event): void
    {
        $redirectRouteResponse = $event->getControllerResult();

        if (!($redirectRouteResponse instanceof RedirectRouteResponse)) {
            return;
        }

        $redirectResponse = new RedirectResponse($this->router->generate(
            $redirectRouteResponse->getRouteName(),
            $redirectRouteResponse->getParameters()
        ), $redirectRouteResponse->getStatusCode(), $redirectRouteResponse->getHeaders());

        $event->setResponse($redirectResponse);
    }
}
